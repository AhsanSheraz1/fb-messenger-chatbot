**Steps:** Following are the steps to test this code:

**1: **Create developer account on developer.facebok.com

**2: **Than create an app for your project.

**3: **Create a facebook page and using the app mentioned in step2 generate tokens against that page.

**4: **clone this project and in messanger/view.py set your **page_access_tokens and verify_tokens(used in webhook).**

**5: **Convert your local connection into secure connection using **ngrok, heroku and etc** with any third party tool.

**6: **Run the project on local system and make it secure using tools mentioned in the above step.

**7: **Now configure webhook with your secure address assign by ngrok and with your verify_tokens mentioned in step4.

